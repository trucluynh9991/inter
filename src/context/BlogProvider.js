import React, {useReducer} from 'react';
import BlogContext from './blogContext';
import blogReduce from './blogReduce';

const BlogProvider = props =>{
    const initialState = {
        blogPost: [],
        currentBlogPost: null,
        loading: true
    }

    const [state, dispatch] = useReducer(blogReduce, initialState)
 //getPost chuc nang se nhan duoc bai dang tren blog
    const getPosts = async () =>{
        try{
            dispatch({ type: 'SENDING_REQUEST'});
            const res = await fetch('/posts');
            const data = await res.json();
            dispatch({ type: 'REQUEST_FINISHED'});
            dispatch({type:'SET_POST', payload: data });
        }
        catch(err){
            console.log(err)
        }
    }

    return(
        <BlogContext.Provider value={{
            blogPost: state.blogPost,
            currentBlogPost: state.currentBlogPost,
            loading: state.loading,
            getPosts: getPosts
        }}>
             {props.children}
        </BlogContext.Provider>
    )
}
export default BlogProvider;