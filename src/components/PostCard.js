import React from 'react'

const PostCard = (props) => {
    return (
        <div className="card">
            <div className="card-image" style={
                {
                    width: '100%',
                    height: '200px',
                    backgroundImage: `url('${props.image}')`,
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    position: 'relative',
                    borderTopLeftRadius: '2px',
                    borderTopRightRadius: '2px'
                }
            }>
            </div>
            <div className="cart-info">
                <div className="cart-title">
                    <p>{props.title}</p>
                </div>
                <div className="cart-author">
                    <p>{props.thumbnailUrls}</p>
                </div>
            </div>
        </div>
    )
}
export default PostCard;