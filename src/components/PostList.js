import React, {useContext, useEffect} from 'react';
import BlogContext from '../context/blogContext';
import PostCard from './PostCard';

const PostList = () => {
    const blogContext = useContext(BlogContext);
    const {getPosts, blogPost, loading} = blogContext;
    useEffect(() => {
     getPosts();
    }, [])
    console.log(blogPost)
    return (
        <div className="post">
           <div className="container">
               <h2>Posts</h2>
               {
                   !loading ? (
                       <div className="post_grid">
                           {
                               blogPost.map((post, i) =>{
                                 return(
                                     <PostCard 
                                        key={i}
                                        title={post.title}
                                        image={post.url}
                                        thumbnailUrl={post.thumbnailUrl}
                                        id={post.id}
                                    />
                                 )
                               })
                           }
                       </div>
                   ) : (
                       <div>Loading...</div>
                   )
               }
           </div>
        </div>
    )
}
export default PostList;
